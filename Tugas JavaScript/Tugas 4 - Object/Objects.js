/*
 * 1. Buatlah variabel dengan nama restaurant yang object dengan ketentuan berikut:
 *    - Memiliki properti bernama "name"
 *       - Bertipe data string
 *       - Bernilai apa pun, asalkan tidak string kosong atau null.
 *    - Memiliki properti bernama "city"
 *       - Bertipe data string
 *       - Bernilai apa pun, asalkan tidak string kosong atau null.
 *    - Memiliki properti "favorite drink"
 *       - Bertipe data string
 *       - Bernilai apa pun, asalkan tidak string kosong atau null.
 *    - Memiliki properti "favorite food"
 *       - Bertipe data string
 *       - Bernilai apa pun, asalkan tidak string kosong atau null.
 *    - Memiliki properti "isVegan"
 *       - Bertipe data boolean
 *       - Bernilai boolean apa pun.
 * 
 * 2. Buatlah variabel bernama name.
 *    Kemudian isi dengan nilai name dari properti object restaurant
 * 
 * 3. Buatlah variabel bernama favoriteDrink.
 *    Kemudian isi dengan nilai "favorite drink" dari properti object restaurant
 * 
*/

console.log('\n');

const restaurant = {
  name: 'Kakarak Abdi',
  city: 'Jakarta',
  favoriteDrink: 'Chocolate Mocha',
  favoriteFood: 'Makaroni',
}

var isVegan = false;
var name = restaurant.name;
var favoriteDrink = restaurant.favoriteDrink;



console.log(`Halo, Nama restaurant ini adalah ${name} \nlokasi berada di ${restaurant.city}`);
console.log(`Minuman favorite disini adalah ${favoriteDrink} dan favorite makanannya ${restaurant.favoriteFood}`);
console.log(`Apakah makanan ini vegan? : ${isVegan}`);
console.log('\n');

module.exports = {restaurant, name, favoriteDrink};
