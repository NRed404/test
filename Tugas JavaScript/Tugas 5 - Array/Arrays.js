/* 
 Buatlah sebuah variable dengan nama nomorGenap yang merupakan sebuah array dengan ketentuan: 
  - Array tersebut menampung bilangan genap dari 1 hingga 100

  Note: 
    - Agar lebih mudah bisa menggunakan for loop dan logika if untuk mengisi array tersebut.
*/

/* Ori gw
function nomorGenap() {
  let arrGenap = [true];
  
  for (let i = 1; i <= 100; i++) {    
    if (i % 2 == 0) {
        console.log(i + ' - Genap')
    } 
  }

  return arrGenap;
}

console.log(nomorGenap());

//Hiraukan kode di bawah ini
module.exports = { nomorGenap };
*/

// KODE YANG SUDAH FIX

function nomorGenap() {
  let arrGenap = [];

  for(let i = 1; i <= 100; i++) {
    if(i % 2 === 0) arrGenap.push(i);
  }

  return arrGenap;
}

module.exports = { nomorGenap };