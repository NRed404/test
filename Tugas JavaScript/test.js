console.log('\n');

const restaurant = {
  name: 'Kakarak Abdi',
  city: 'Jakarta',
  favoriteDrink: 'Chocolate Mocha',
  favoriteFood: 'Makaroni',
}

var isVegan = false;
var name = restaurant.name;
var favoriteDrink = restaurant.favoriteDrink;



console.log(`Halo, Nama restaurant ini adalah ${name} \nlokasi berada di ${restaurant.city}`);
console.log(`Minuman favorite disini adalah ${favoriteDrink} dan favorite makanannya ${restaurant.favoriteFood}`);
console.log(`Apakah makanan ini vegan? : ${isVegan}`);
console.log('\n');

module.exports = {restaurant, name, favoriteDrink};
